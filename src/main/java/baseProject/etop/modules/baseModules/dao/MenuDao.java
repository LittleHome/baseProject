package baseProject.etop.modules.baseModules.dao;

import baseProject.etop.basic.dao.BaseDao;
import baseProject.etop.modules.baseModules.entity.TMenuEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by jessy on 2015/3/12.
 */
@Repository("MenuDao")
public class MenuDao extends BaseDao<TMenuEntity>{
}
