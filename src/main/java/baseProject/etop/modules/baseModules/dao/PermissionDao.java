package baseProject.etop.modules.baseModules.dao;

import baseProject.etop.basic.dao.BaseDao;
import baseProject.etop.modules.baseModules.entity.TPermissionEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by jessy on 2015/3/8.
 */
@Repository("permissionDao")
public class PermissionDao extends BaseDao<TPermissionEntity>{
}
