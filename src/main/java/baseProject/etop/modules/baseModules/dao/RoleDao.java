package baseProject.etop.modules.baseModules.dao;

import baseProject.etop.basic.dao.BaseDao;
import baseProject.etop.modules.baseModules.entity.TRoleEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by pcc on 2015/3/8.
 */
@Repository("roleDao")
public class RoleDao extends BaseDao<TRoleEntity>{
}
