package baseProject.etop.modules.baseModules.dao;

import baseProject.etop.basic.dao.BaseDao;
import baseProject.etop.modules.baseModules.entity.TUserEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by jessy on 2015/3/5.
 */
@Repository("userDao")
public class UserDao extends BaseDao<TUserEntity>{
}
